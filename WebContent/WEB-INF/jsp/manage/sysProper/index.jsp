<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle}</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
		<style type="text/css">
		body{}
		table{}
		table tr th{
			width:200px;
			height:30px;
			vertical-align:middle;
			text-align:left;
			font-weight:bold;
		}
		table tr td{
			width:200px;
			vertical-align:middle;
			text-align:center;
			margin:2 3 2 3;
		}
		table tr td input{
			width:190px;
		}
		table tr td select {
			width:194px;
		}
		</style>
	</head>

	<body>
	<span>该属性配置页面用来对系统基本信息进行配置,每项信息更新后,信息所在文本框或下拉框焦点移开即可完成数据更新,因此,在更新相关数据时,请慎重.</span>
	<br/>
	<table>
	<c:forEach items="${map}" var="prop">
		<tr>
			<th>${prop.key}</th><td>${prop.value}</td>
		</tr>
	</c:forEach>
	</table>
	</body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
		bindEvents();
	});
	function bindEvents(){
		$("input[mthod='smt']").bind("blur",function(e){submit(e)});
		$("select[mthod='smt']").bind("blur",function(e){submit(e)});
	};	
	function submit(e){
			$.ajax({
				 type: "POST",
				 url: path+"/Manage/SysProper/doUpdate.do",
				 data: eval("[{key:'"+$(e.target).attr("name")+"',value:'"+$(e.target).val()+"'}]")[0],
				 dataType: "text",
				 async:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							//$("<div style=\"position:absolute;left:0;top:0;\">"+t.msg+"</div>").appendTo("body");
						}else{
							alert(t.msg);
						}
					}
				}
			 });
		return true;
	}
</script></html>
