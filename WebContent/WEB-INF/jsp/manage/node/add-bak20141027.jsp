<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:500px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:30%;
		} 
		.easyui-tabs table tr .val{
			width:60%;
		} 
		.easyui-tabs table tr .val input{
			margin:2 3 2 3;
			width:96%;
		} 
		.easyui-tabs table tr .val select{
			margin:2 3 2 3;
			width:96%;
		} 
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/entity/node.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;">
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="基本信息">
    	<input type="hidden" id="entityId" name="id" value="${entity.id }"/>
    	<input type="hidden" name="parentId" id="parentId" value="${parentId }"/>
    	<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="tit">栏目状态</td>
				<td class="val">
					<select mthod="smt" name="state">
						<option value="-1" <c:if test="${entity.state==-1 }">selected</c:if>>锁定</option>
						<option value="1" <c:if test="${entity.state==1 }">selected</c:if>>开通</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目名称</td>
				<td class="val">
					<input type="text" mthod="smt" name="text" datatype="*3-10" errormsg="名称长度不得小于3且不得大于10" value="${entity.text }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目类型</td>
				<td class="val">
					<select mthod="smt" name="nodeType">
						<option value="0" <c:if test="${entity.nodeType==0 }">selected</c:if>>普通栏目</option>
						<option value="1" <c:if test="${entity.nodeType==1 }">selected</c:if>>转向栏目</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目分组</td>
				<td class="val">
					<input type="text" mthod="smt" name="group" value="${entity.group }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">链接地址</td>
				<td class="val">
					<input type="text" mthod="smt" name="linkUrl" value="${entity.linkUrl }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">关键字</td>
				<td class="val">
					<input type="text" mthod="smt" name="keywords" value="${entity.keywords }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">排序值</td>
				<td class="val">
					<input type="text" mthod="smt" name="order" value="${entity.order }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目模型</td>
				<td class="val">
					<select mthod="smt" name="nodeMediaId">
						<option value="-1" <c:if test="${entity.nodeMediaId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">内容模型</td>
				<td class="val">
					<select mthod="smt" name="contentMediaId">
						<option value="-1" <c:if test="${entity.contentMediaId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">内容统计单位</td>
				<td class="val">
					<input type="text" mthod="smt" name="subContentCountUnit" value="${entity.subContentCountUnit }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目说明</td>
				<td class="val">
					<input type="text" mthod="smt" name="description" value="${entity.description }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目管理员</td>
				<td class="val">
					<input type="text" mthod="smt" name="manageUser" value="${entity.manageUser }"/>
				</td>
			</tr>
		</table>
    	
    </div>
    <div title="展现设置">
 	  	<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="tit">导航中显示</td>
				<td class="val">
					<select mthod="smt" name="displayInNavigator">
						<option value="true" <c:if test="${entity.displayInNavigator==true }">selected</c:if>>是</option>
						<option value="false" <c:if test="${entity.displayInNavigator==false }">selected</c:if>>否</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">导航排序</td>
				<td class="val">
					<input type="text" mthod="smt" name="orderByNavigator" value="${entity.orderByNavigator }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">代表图片</td>
				<td class="val">
					<input type="text" mthod="smt" name="map" value="${entity.map }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">背景音乐</td>
				<td class="val">
					<input type="text" mthod="smt" name="bgMusic" value="${entity.bgMusic }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目logo</td>
				<td class="val">
					<input type="text" mthod="smt" name="logo" value="${entity.logo }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目图标</td>
				<td class="val">
					<input type="text" mthod="smt" name="icon" value="${entity.icon }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目banner</td>
				<td class="val">
					<input type="text" mthod="smt" name="banner" value="${entity.banner }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目版权</td>
				<td class="val">
					<input type="text" mthod="smt" name="copyright" value="${entity.copyright }"/>
				</td>
			</tr>
 	  	</table>
    </div>
    <div title="模板信息">
 	  	<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="tit">使用模板库</td>
				<td class="val">
					<select mthod="smt" name="templateGroupId">
						<option value="-1" <c:if test="${entity.templateGroupId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目页面模板</td>
				<td class="val">
					<select mthod="smt" name="nodePageTemplateId">
						<option value="-1" <c:if test="${entity.nodePageTemplateId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">栏目样式模板</td>
				<td class="val">
					<select mthod="smt" name="nodeStyleTemplateId">
						<option value="-1" <c:if test="${entity.nodeStyleTemplateId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">内容页面模板</td>
				<td class="val">
					<select mthod="smt" name="contentpageTemplateId">
						<option value="-1" <c:if test="${entity.contentpageTemplateId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">内容样式模板</td>
				<td class="val">
					<select mthod="smt" name="contentStyleTemplateId">
						<option value="-1" <c:if test="${entity.contentStyleTemplateId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">评论页面模板</td>
				<td class="val">
					<select mthod="smt" name="commentpageTemplateId">
						<option value="-1" <c:if test="${entity.commentpageTemplateId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">评论样式模板</td>
				<td class="val">
					<select mthod="smt" name="commentStyleTemplateId">
						<option value="-1" <c:if test="${entity.commentStyleTemplateId==-1 }">selected</c:if>>继承上级</option>
					</select>
				</td>
			</tr>
		</table>
    </div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(function(){
	});
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	function submit(){
		if($("#parentId").val()==""){
			alert("参数错误");
			return false;
		}
		
		
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			if(_s_tmp[i].tagName=="SELECT"){
				params+=""+(_s_tmp[i].name)+":'"+_s_tmp[i].selectedOptions[0].value+"',";
			}else if(_s_tmp[i].tagName=="INPUT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";
			}else{
			}
		} 
		params="[{"+params+"parentId:"+$("#parentId").val()+"}]";
		
		$.post(
				"<%=path %>/Manage/Node/doAdd.do",
				eval(params)[0],
				function(result){
				//	alert(result);
					//rtn=true;
				}
			);
		return true;
	}
</script>