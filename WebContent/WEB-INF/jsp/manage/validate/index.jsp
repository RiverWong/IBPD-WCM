<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>验证模型管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
		<div region="west" border="true">


            <table id="grid" style="width: 900px" title="用户操作" iconcls="icon-view">            
            </table>


		</div>
	</div>
	<div id="mm" class="easyui-menu" style="width:120px;">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除</div>
	    <div onClick="bind()" data-options="iconCls:'icon-view'">查看绑定</div>
	    <div class="menu-sep"></div>
	    <div onClick="print()" data-options="iconCls:'icon-print'">打印</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新</div>
	</div>
	<div id="addDialog" class="easyui-window"  title="修改密码" collapsible="false" minimizable="false"
        maximizable="false" icon="icon-save"  style="width: 800px; height: 400px; padding: 5px;
        background: #fafafa;">
		<jsp:include page="/manage/validate/add.jsp"></jsp:include>
	</div>
	<script type="text/javascript">
	//实现对DataGird控件的绑定操作
        function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>manage/validate/list.do',   //指向后台的Action来获取当前菜单的信息的Json格式的数据
                title: '验证模型列表',
                iconCls: 'icon-grid',
                singleSelect:false,
                height: 650,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                autoRowHeight: false,
                striped: true,
                collapsible: true,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
				    $('#mm').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
                columns: [[
                    { field: 'ck', checkbox: true },   //选择                   
                     { title: '模型名称', field: 'validateNamee', width: 150, sortable:true },
                     { title: '模型类别', field: 'validateType', width: 80, sortable:true },
                     { title: '验证URL地址', field: 'ajaxUrl', width: 300, sortable:true},
                     { title: '创建时间', field: 'createTime', width: 150, sortable:true,formatter: function (val, rowdata, index){var _s=eval(val);return _s.year+"-"+_s.month+"-"+_s.day;} },
                     { title: '验证表达式', field: 'dataType', width: 150, sortable:true },
                     { title: '排序', field: 'order', width: 60, sortable:true },
                     { title: '关联标签id', field: 'rechek', width: 80, sortable:true },
                     { title: '操作', field: 'id',  width:120,align:'center',formatter: function (val, rowdata, index) {return '<img style="width:16px;height:16px;padding-right:5px;" src="<%=basePath %>images/edit.jpg"/><img style="width:16px;height:16px;" src="<%=basePath %>images/del.jpg"/>';}},
                     { title: '附加js', field: 'func', width: 80, sortable:true,hidden:true },
                     { title: '错误提示', field: 'errormsg', width: 80, sortable:true,hidden:true }
               ]], 
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditOrViewDialog();//实现修改记录的方法
                    }
                }, '-', {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                }, '-', {
                    id: 'btnView',
                    text: '查看绑定信息',
                    iconCls: 'icon-view',
                    handler: function () {
                        bind();//实现查看记录详细信息的方法
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    ShowEditOrViewDialog();
                }
            })
        };
        function bind(){
        	$.messager.alert('提示','未开发');
        };
        function reload(){
        	$("#grid").datagrid("reload");
        };
        function print(){
        	$.messager.alert('提示','未开发');
        };
        function ShowAddDialog(){
        	setFieldValue('','','','','','','','');
        	$('#addDialog').window('open');
        };
        function setFieldValue(id,validateNamee,validateType,dataType,ajaxUrl,errormsg,func,rechek){
        		$("#id").val(id);
				$("#validateName").val(validateNamee);
				$("#validateType").val(validateType);
				$("#dataType").val(dataType);
				$("#ajaxUrl").val(ajaxUrl);
				$("#errormsg").val(errormsg);
				$("#func").val(func);
				$("#rechek").val(rechek); 
        
        };
        function ShowEditOrViewDialog(){
        	 var row = $('#grid').datagrid('getSelected');
			if (row){	
			$("#fm").attr("action","<%=basePath%>manage/validate/doEdit.do");
				setFieldValue(row.id,row.validateNamee,row.validateType,row.dataType,row.ajaxUrl,row.errormsg,row.func,row.rechek);
				$('#addDialog').window('open');
			}else{
				$.messager.alert('提示','请选择记录后再操作');
			}
        };
        function del(){
	        var row = $('#grid').datagrid('getSelections');
	        if(row.length>0){
	        	$.messager.confirm('提示','确定删除?',function(r){
		        	if(r){
			        	var ids="";
			        	$.each(row, function (index, item) {
		            		ids += item.id + ",";
		        		});
		        		ids=ids.substring(0,ids.length-1);
			        	location.href="<%=basePath %>manage/validate/doDel.do?ids="+ids;
		        	}
	        	});
	        }else{
	        	$.messager.alert('提示','没有选中记录');
	        }
        };
        $(function(){
        	InitGrid("id=1");
        	$('#addDialog').window({
                title: '添加新的验证模型',
                width: 800,
                modal: true,
                shadow: true,
                closed: true,
                height: 400,
                resizable:false
            });
        	$('#addDialog').window('close');
        });
	</script>
	</body>
</html>
