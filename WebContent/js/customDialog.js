$.fn.customDialog = function(options) {
	var defaults = {
		modal:true,
        title:'更新',
        shadow:true,
        iconCls:'icon-edit',
        width:600,
        height:500,
        resizable:true,	
	}
	var id = $(this).attr('id');
	options = $.extend(defaults, options);
	var self = this;

	$(self).dialog({
		title: options.title,
		height: options.height,
		width: options.width,
		iconCls: options.iconCls,
		toolbar:[{
            text:options.processButtonText,
            iconCls:'icon-ok',
            handler:function(){
                if($("#editiframe")[0].contentWindow.submit()){
	                $("#"+id).dialog("close");
					$("#"+id).remove();
					reload(options.gridId);
                }else{
                }
            }
        },'-',{
            text:'取消',
            iconCls:'icon-cancel',
            handler:function(){
                $("#"+id).dialog("close");
				$("#"+id).remove();
            }
		}],
		content:'<iframe id="editiframe" width="90%" height="90%" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+options.url+'"></iframe>'
	});
}