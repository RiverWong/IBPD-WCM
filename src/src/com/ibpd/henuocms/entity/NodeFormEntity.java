package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 栏目表单
 * @author MG
 * @version 1.0
 */ 
@Entity
@Table(name="T_NodeForm")
public class NodeFormEntity  extends IBaseEntity{
	
	public static Integer FORMTYPE_NODEFORM=0;
	public static Integer FORMTYPE_CONTENTFORM=1;
	/**
	 * 自定义表单,用来前台展现 自定义表单（如招聘等）
	 */
	public static Integer FORMTYPE_CUSTOM=2;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 表单名称
	 */
	@Column(name="f_formName",length=50,nullable=true)
	private String formName;
	/**
	 * 表单类别(栏目表单或内容表单,将来可能扩展为自定义表单，但没想好具体细节的实现)
	 */
	@Column(name="f_formType",nullable=true)
	private Integer formType=FORMTYPE_NODEFORM;
	/**
	 * 栏目ID，指当前表单是哪个栏目下创建的，以及直接使用于哪个栏目，这个栏目具有对该表单的处置权限
	 */
	@Column(name="f_nodeId",nullable=true)
	private Long nodeId=-1L;
	/**=
	 * 是否默认
	 */
	@Column(name="f_isDefault",nullable=true)
	private Boolean isDefault;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updateTime",nullable=true)
	private Date updateTime=new Date();//更新时间
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setFormType(Integer formType) {
		this.formType = formType;
	}
	public Integer getFormType() {
		return formType;
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public Long getNodeId() {
		return nodeId;
	}
	
}