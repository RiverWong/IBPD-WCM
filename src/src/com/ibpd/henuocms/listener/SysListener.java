package com.ibpd.henuocms.listener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.ibpd.henuocms.timmer.InitJob;
/**
 * 系统listener，暂时废弃
 * @author mg by qq:349070443
 * 
 */
public class SysListener implements ServletContextListener {
 public static final Logger log = Logger.getLogger("HsqlListener");
 
 public static final String TOKEN = "${webapp.root}";
 
 public static final int WAIT_TIME = 2000;
 
 private String url;
 
 private String username;
 
 private String password;
      
     public void contextInitialized(ServletContextEvent sce) { 
//         try { 
//           username = "root";
//           password = "root";
//           String databaseName = "henuoCMS";
//           int port = 9099; 
//           String hsqlPath = TOKEN+"/hsqldb";
//  
//             // FIXME: 因为要用到getRealPath方法获得路径，在使用war包发布的时候会出现问题 
//             if (hsqlPath.startsWith(TOKEN)) { 
//                 String webappRoot = sce.getServletContext().getRealPath("/").replace("\\", "/"); 
//                 hsqlPath = hsqlPath.substring(TOKEN.length()); 
//                 hsqlPath = webappRoot + hsqlPath;
//             }
//             String databasePath = hsqlPath + "/" + databaseName; 
//             url = "jdbc:hsqldb:hsql://localhost:" + port + "/" + databaseName; 
//             Server server = new Server(); 
//             server.setDatabaseName(0, databaseName);
//             server.setDatabasePath(0, databasePath); 
//             server.setPort(port); 
//             server.setSilent(true);
//             server.start();
//             Thread.sleep(WAIT_TIME); 
//             log.info("Hsqldb启动成功！");
//         } catch (Exception ex) { 
//          log.error("Hsqldb启动失败：" + ex); 
//         } 
     } 
  
      
     public void contextDestroyed(ServletContextEvent sce) { 
    	 InitJob.stopServer();
//         try { 
//             Class.forName("org.hsqldb.jdbcDriver"); 
//             Connection conn = null; 
//             Statement state = null; 
//             try {
//                 // 向数据库发送shutdown命令，关闭数据库 
//                 conn = DriverManager.getConnection(url, username, password); 
//                 state = conn.createStatement(); 
//                 state.executeUpdate("SHUTDOWN;");
//                 try {
//     Thread.sleep(WAIT_TIME);
//    } catch (InterruptedException e) {
//     e.printStackTrace();
//    }
//    log.info("关闭hsqldb数据库成功！");
//             } catch (SQLException ex1) { 
//                 log.error("关闭hsqldb数据库时出现异常：" + ex1); 
//             } finally { 
//                 // 确保关闭Statement 
//                 if (state != null) { 
//                     try { 
//                         state.close(); 
//                         state = null; 
//                     } catch (SQLException ex1) { 
//                      log.error("关闭Statement时异常："+ex1); 
//                     } 
//                 } 
//                 // 确保关闭Connection 
//                 if (conn != null) { 
//                     try { 
//                         conn.close(); 
//                         conn = null; 
//                     } catch (SQLException ex1) { 
//                         log.error("关闭Connection时异常："+ex1); 
//                     } 
//                 } 
//             } 
//         } catch (ClassNotFoundException ex) { 
//             log.error("HsqldbListener : contextDestoryed : error : " + ex); 
//         } 
     } 
}