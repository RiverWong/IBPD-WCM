package com.ibpd.henuocms.service.config;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.entity.ConfigEntity;
@Service("configService")
public class ConfigServiceImpl extends BaseServiceImpl<ConfigEntity> implements IConfigService {
	public ConfigServiceImpl(){
		super();
		this.tableName="ConfigEntity";
		this.currentClass=ConfigEntity.class;
		this.initOK();
	}
	/**
	 * 偷懒用的，没用了
	 * @param args
	 */
	public static void main(String[] args){
		Map mKey=new HashMap<String,String>();
		Map<String,String> mVal=new HashMap<String,String>();
		mKey.put("siteName","站点名称");
		mKey.put("siteKeys","站点关键字");
		mKey.put("siteIntro","站点说明");
		mKey.put("siteStatus","站点状态");
		mKey.put("siteOpenRule","站点开启规则（永久开启、定时开启）");
		mKey.put("siteOpenType","定时开启方式（每天、每周、每月、每年）");
		mKey.put("siteOpenTime","定时开启日期(每天：X时X分；每周：1234567；每月：1-31；每年：X月X日）");
		mKey.put("siteCloseRule","站点关闭规则（永久开启、定时开启）");
		mKey.put("siteCloseType","定时关闭方式（每天、每周、每月、每年）");
		mKey.put("siteCloseTime","定时关闭日期(每天：X时X分；每周：1234567；每月：1-31；每年：X月X日）");
		mKey.put("makeStaticPage","自动生成静态页（自动、手动生成）");
		mKey.put("staticRootPage","首页静态化");
		mKey.put("staticNodePage","栏目页静态化");
		mKey.put("staticContentPage","内容页静态化");
		mKey.put("staticFormPage","表单页静态化");
		mKey.put("staticOtherPage","其他页面静态化");
		mKey.put("pageTemplateId","使用模板id");
		mKey.put("addIimageWatemark","上传图片是否加水印");
		mKey.put("watemarkImage","水印图片");
		mKey.put("watemarkPosition","水印位置");
		mKey.put("watemarkOpacity","水印透明度");
		mKey.put("noAddWatemarkImg","不加水印图片的大小");
		mKey.put("siteGray","全站变灰");
		mKey.put("imageAutoCompress","上传图片自动压缩（自动、手动）");
		mKey.put("imageAutoConvert","上传图片自动转换格式");
		mKey.put("videoAutoConvToFlv"," 上传视频自动转换为FLV格式");
		mKey.put("timingBakSite"," 定期自动备份网站文件");
		mKey.put("timingBakSiteType","定时执行方式（一次性、每天、每周、每月、每年）");
		mKey.put("timingBakSiteTime","定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）");
		mKey.put("timingBakDb","定期自动备份网站数据库信息");
		mKey.put("timingBakDbType","定时执行方式（一次性、每天、每周、每月、每年）");
		mKey.put("timingBakDbTime","定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）");
		mKey.put("timingClearDb","定期清理数据库");
		mKey.put("timingClearDbType","定时执行方式（一次性、每天、每周、每月、每年）");
		mKey.put("timingClearDbTime","定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）");
		mKey.put("dbType","连接数据库类型");
		mKey.put("dbUrl"," 数据库连接URL");
		mKey.put("dbName","数据库名称");
		mKey.put("dbUserName","数据库用户名");
		mKey.put("dbPassword","数据库密码");
		mKey.put("autoPublishFtpServer","信息是否自动发布到FTP服务器");
		mKey.put("ftpAddress"," FTP地址");
		mKey.put("ftpPort"," FTP端口号");
		mKey.put("ftpUserName"," FTP用户名");
		mKey.put("ftpPassword"," FTP密码");
		mKey.put("webRootPath"," FTP网站根目录");
		mKey.put("isEnableStatistics","是否开启站点访问量统计");

		Set<String> ks=mKey.keySet();
		Iterator<String> it=ks.iterator();
		while(it.hasNext()){
			String key=it.next();
			String sp[]={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
			String ss[]={};
			String tmp="";
			for(Integer i=0;i<sp.length;i++){
				ss=new String[]{};
				ss=key.split(sp[i]);
				if(ss.length==2){
					tmp=sp[i];
					break;
				}
			}
			if(ss.length==2){
				tmp=(ss[0]+"_"+tmp+ss[1]).toUpperCase();
//				System.out.println("public  static final String "+tmp+" = \""+key+"\";");
				IbpdLogger.getLogger(ConfigServiceImpl.class).info("mKey.put("+tmp+",\""+mKey.get(key)+"\");");
			}
		}
	}	
	public  static final String TIMING_BAKDBTYPE = "timingBakDbType";
	public  static final String WATEMARK_IMAGE = "watemarkImage";
	public  static final String IMAGE_AUTOCONVERT = "imageAutoConvert";
	public  static final String FTPUSER_NAME = "ftpUserName";
	public  static final String ADD_IIMAGEWATEMARK = "addIimageWatemark";
	public  static final String TIMING_CLEARDBTYPE = "timingClearDbType";
	public  static final String DB_NAME = "dbName";
	public  static final String MAKESTATIC_PAGE = "makeStaticPage";
	public  static final String SITE_OPENTIME = "siteOpenTime";
	public  static final String SITE_OPENRULE = "siteOpenRule";
	public  static final String STATIC_FORMPAGE = "staticFormPage";
	public  static final String PAGETEMPLATE_ID = "pageTemplateId";
	public  static final String DB_TYPE = "dbType";
	public  static final String SITE_NAME = "siteName";
	public  static final String TIMING_CLEARDBTIME = "timingClearDbTime";
	public  static final String SITE_OPENTYPE = "siteOpenType";
	public  static final String WATEMARK_OPACITY = "watemarkOpacity";
	public  static final String STATICROOT_PAGE = "staticRootPage";
	public  static final String STATIC_OTHERPAGE = "staticOtherPage";
	public  static final String STATIC_NODEPAGE = "staticNodePage";
	public  static final String VIDEO_AUTOCONVTOFLV = "videoAutoConvToFlv";
	public  static final String WATEMARK_POSITION = "watemarkPosition";
	public  static final String TIMING_BAKSITETYPE = "timingBakSiteType";
	public  static final String FTP_ADDRESS = "ftpAddress";
	public  static final String SITE_INTRO = "siteIntro";
	public  static final String TIMING_BAKSITE = "timingBakSite";
	public  static final String TIMING_CLEARDB = "timingClearDb";
	public  static final String IMAGE_AUTOCOMPRESS = "imageAutoCompress";
	public  static final String DB_PASSWORD = "dbPassword";
	public  static final String TIMING_BAKSITETIME = "timingBakSiteTime";
	public  static final String AUTOPUBLISH_FTPSERVER = "autoPublishFtpServer";
	public  static final String DB_URL = "dbUrl";
	public  static final String SITE_CLOSETYPE = "siteCloseType";
	public  static final String SITE_GRAY = "siteGray";
	public  static final String FTP_PASSWORD = "ftpPassword";
	public  static final String SITE_CLOSERULE = "siteCloseRule";
	public  static final String SITE_KEYS = "siteKeys";
	public  static final String STATIC_CONTENTPAGE = "staticContentPage";
	public  static final String NO_ADDWATEMARKIMG = "noAddWatemarkImg";
	public  static final String FTP_PORT = "ftpPort";
	public  static final String DBUSER_NAME = "dbUserName";
	public  static final String WEBROOT_PATH = "webRootPath";
	public  static final String TIMING_BAKDB = "timingBakDb";
	public  static final String SITE_CLOSETIME = "siteCloseTime";
	public  static final String TIMING_BAKDBTIME = "timingBakDbTime";
	public  static final String SITE_STATUS = "siteStatus";
	public  static final String IS_ENABLESTATISTICS = "isnableStatistics";

	public enum TIMING_BAKDBTYPE_ENUM{
		close,//关闭 不自动备份
		only,//一次性
		everyday,//每天
		everyweek,//每周
		everymonth,//每月
		everyyear//每年
	}
	public enum SITE_OPENRULE_ENUM{
		/**
		 * 永久开启
		 */
		timmingOpen,//永久开启
		/**
		 * 定时开启-永不关闭
		 */
		timmingOpenAndNoClose,//定时开启-永不关闭
		/**
		 * 定时开启-定时关闭
		 */
		timmingOpenAndTimmingClose,//定时开启-定时关闭
		/**
		 * 定时关闭
		 */
		timmingClose//定时关闭
	}
	public final String DEFAULT_WATEMARK_IMG="/common/images/defaultWateMark.png";
	public final String DEFAULT_0="0";
	public final String DEFAULT_1="1";
	public final String DEFAULT_SITE_STATUS="1";
	public final String SITE_STATUS_OPEN=DEFAULT_SITE_STATUS;
	public final String SITE_STATUS_CLOSED="0";
	public void InitSysConfigParams() {
		System.err.println("清空配置表");
//		List<ConfigEntity> cList=this.getList();
		this.getDao().executeSqlString("delete from t_config", ConfigEntity.class);
		Map<String,String> mKey=new HashMap<String,String>();
		Map<String,String> mVal=new HashMap<String,String>();
		Map<String,String> mRem=new HashMap<String,String>();
		mKey.put(TIMING_BAKDBTYPE,"定时执行方式");
		mKey.put(WATEMARK_IMAGE,"水印图片");
		mKey.put(IMAGE_AUTOCONVERT,"上传图片自动转换格式");
		mKey.put(FTPUSER_NAME," FTP用户名");
		mKey.put(ADD_IIMAGEWATEMARK,"上传图片是否加水印");
		mKey.put(TIMING_CLEARDBTYPE,"定时执行方式");
		mKey.put(DB_NAME,"数据库名称");
		mKey.put(MAKESTATIC_PAGE,"自动生成静态页");
		mKey.put(SITE_OPENTIME,"定时开启日期");
		mKey.put(SITE_OPENRULE,"站点开启规则");
		mKey.put(STATIC_FORMPAGE,"表单页静态化");
		mKey.put(PAGETEMPLATE_ID,"使用模板id");
		mKey.put(DB_TYPE,"连接数据库类型");
		mKey.put(SITE_NAME,"站点名称");
		mKey.put(TIMING_CLEARDBTIME,"定时执行日期");
		mKey.put(SITE_OPENTYPE,"定时开启方式");
		mKey.put(WATEMARK_OPACITY,"水印透明度");
		mKey.put(STATICROOT_PAGE,"首页静态化");
		mKey.put(STATIC_OTHERPAGE,"其他页面静态化");
		mKey.put(STATIC_NODEPAGE,"栏目页静态化");
		mKey.put(VIDEO_AUTOCONVTOFLV," 上传视频自动转换为FLV格式");
		mKey.put(WATEMARK_POSITION,"水印位置");
		mKey.put(TIMING_BAKSITETYPE,"定时执行方式");
		mKey.put(FTP_ADDRESS," FTP地址");
		mKey.put(SITE_INTRO,"站点说明");
		mKey.put(TIMING_BAKSITE," 定期自动备份网站文件");
		mKey.put(TIMING_CLEARDB,"定期清理数据库");
		mKey.put(IMAGE_AUTOCOMPRESS,"上传图片自动压缩（自动、手动）");
		mKey.put(DB_PASSWORD,"数据库密码");
		mKey.put(TIMING_BAKSITETIME,"定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）");
		mKey.put(AUTOPUBLISH_FTPSERVER,"信息是否自动发布到FTP服务器");
		mKey.put(DB_URL," 数据库连接URL");
		mKey.put(SITE_CLOSETYPE,"定时关闭方式（每天、每周、每月、每年）");
		mKey.put(SITE_GRAY,"全站变灰");
		mKey.put(FTP_PASSWORD," FTP密码");
		mKey.put(SITE_CLOSERULE,"站点关闭规则");
		mKey.put(SITE_KEYS,"站点关键字");
		mKey.put(STATIC_CONTENTPAGE,"内容页静态化");
		mKey.put(NO_ADDWATEMARKIMG,"不加水印图片的大小");
		mKey.put(FTP_PORT," FTP端口号");
		mKey.put(DBUSER_NAME,"数据库用户名");
		mKey.put(WEBROOT_PATH," FTP网站根目录");
		mKey.put(TIMING_BAKDB,"定期自动备份网站数据库信息");
		mKey.put(SITE_CLOSETIME,"定时关闭日期");
		mKey.put(TIMING_BAKDBTIME,"定时执行日期");
		mKey.put(SITE_STATUS,"站点状态");
		mKey.put(IS_ENABLESTATISTICS,"是否开启站点访问量统计");

		mVal.put(TIMING_BAKDBTYPE,TIMING_BAKDBTYPE_ENUM.close.toString());
		mVal.put(WATEMARK_IMAGE,DEFAULT_WATEMARK_IMG);
		mVal.put(IMAGE_AUTOCONVERT,DEFAULT_1);
		mVal.put(FTPUSER_NAME,StringUtils.EMPTY);
		mVal.put(ADD_IIMAGEWATEMARK,DEFAULT_1);
		mVal.put(TIMING_CLEARDBTYPE,TIMING_BAKDBTYPE_ENUM.close.toString());
		mVal.put(DB_NAME,StringUtils.EMPTY);
		mVal.put(MAKESTATIC_PAGE,DEFAULT_1);
		mVal.put(SITE_OPENTIME,StringUtils.EMPTY);
		mVal.put(SITE_OPENRULE,SITE_OPENRULE_ENUM.timmingOpen.toString());
		mVal.put(STATIC_FORMPAGE,DEFAULT_1);
		mVal.put(PAGETEMPLATE_ID,"-1");
		mVal.put(DB_TYPE,StringUtils.EMPTY);
		mVal.put(SITE_NAME,"IBPD演示站");
		mVal.put(TIMING_CLEARDBTIME,StringUtils.EMPTY);
		mVal.put(SITE_OPENTYPE,TIMING_BAKDBTYPE_ENUM.close.toString());
		mVal.put(WATEMARK_OPACITY,"0.7");
		mVal.put(STATICROOT_PAGE,DEFAULT_1);
		mVal.put(STATIC_OTHERPAGE,DEFAULT_1);
		mVal.put(STATIC_NODEPAGE,DEFAULT_1);
		mVal.put(VIDEO_AUTOCONVTOFLV,DEFAULT_1);
		mVal.put(WATEMARK_POSITION,StringUtils.EMPTY);
		mVal.put(TIMING_BAKSITETYPE,TIMING_BAKDBTYPE_ENUM.close.toString());
		mVal.put(FTP_ADDRESS,StringUtils.EMPTY);
		mVal.put(SITE_INTRO,StringUtils.EMPTY);
		mVal.put(TIMING_BAKSITE,DEFAULT_1);
		mVal.put(TIMING_CLEARDB,DEFAULT_1);
		mVal.put(IMAGE_AUTOCOMPRESS,DEFAULT_1);
		mVal.put(DB_PASSWORD,DEFAULT_1);
		mVal.put(TIMING_BAKSITETIME,DEFAULT_1);
		mVal.put(AUTOPUBLISH_FTPSERVER,DEFAULT_1);
		mVal.put(DB_URL,StringUtils.EMPTY);
		mVal.put(SITE_CLOSETYPE,TIMING_BAKDBTYPE_ENUM.close.toString());
		mVal.put(SITE_GRAY,DEFAULT_0);
		mVal.put(FTP_PASSWORD,StringUtils.EMPTY);
		mVal.put(SITE_KEYS,StringUtils.EMPTY);
		mVal.put(STATIC_CONTENTPAGE,DEFAULT_1);
		mVal.put(NO_ADDWATEMARKIMG,"400*300");
		mVal.put(FTP_PORT,StringUtils.EMPTY);
		mVal.put(DBUSER_NAME,StringUtils.EMPTY);
		mVal.put(WEBROOT_PATH,StringUtils.EMPTY);
		mVal.put(TIMING_BAKDB,DEFAULT_1);
		mVal.put(SITE_CLOSETIME,StringUtils.EMPTY);
		mVal.put(TIMING_BAKDBTIME,StringUtils.EMPTY);
		mVal.put(SITE_STATUS,DEFAULT_SITE_STATUS);
		mVal.put(IS_ENABLESTATISTICS,DEFAULT_1);

		mRem.put(TIMING_BAKDBTYPE,"定时执行方式");
		mRem.put(WATEMARK_IMAGE,"水印图片");
		mRem.put(IMAGE_AUTOCONVERT,"上传图片自动转换格式");
		mRem.put(FTPUSER_NAME," FTP用户名");
		mRem.put(ADD_IIMAGEWATEMARK,"上传图片是否加水印");
		mRem.put(TIMING_CLEARDBTYPE,"定时执行方式");
		mRem.put(DB_NAME,"数据库名称");
		mRem.put(MAKESTATIC_PAGE,"自动生成静态页");
		mRem.put(SITE_OPENTIME,"定时开启日期");
		mRem.put(SITE_OPENRULE,"站点开启规则");
		mRem.put(STATIC_FORMPAGE,"表单页静态化");
		mRem.put(PAGETEMPLATE_ID,"使用模板id");
		mRem.put(DB_TYPE,"连接数据库类型");
		mRem.put(SITE_NAME,"站点名称");
		mRem.put(TIMING_CLEARDBTIME,"定时执行日期");
		mRem.put(SITE_OPENTYPE,"定时开启方式");
		mRem.put(WATEMARK_OPACITY,"水印透明度");
		mRem.put(STATICROOT_PAGE,"首页静态化");
		mRem.put(STATIC_OTHERPAGE,"其他页面静态化");
		mRem.put(STATIC_NODEPAGE,"栏目页静态化");
		mRem.put(VIDEO_AUTOCONVTOFLV," 上传视频自动转换为FLV格式");
		mRem.put(WATEMARK_POSITION,"水印位置");
		mRem.put(TIMING_BAKSITETYPE,"定时执行方式");
		mRem.put(FTP_ADDRESS," FTP地址");
		mRem.put(SITE_INTRO,"站点说明");
		mRem.put(TIMING_BAKSITE," 定期自动备份网站文件");
		mRem.put(TIMING_CLEARDB,"定期清理数据库");
		mRem.put(IMAGE_AUTOCOMPRESS,"上传图片自动压缩");
		mRem.put(DB_PASSWORD,"数据库密码");
		mRem.put(TIMING_BAKSITETIME,"定时执行日期");
		mRem.put(AUTOPUBLISH_FTPSERVER,"信息是否自动发布到FTP服务器");
		mRem.put(DB_URL," 数据库连接URL");
		mRem.put(SITE_CLOSETYPE,"定时关闭方式");
		mRem.put(SITE_GRAY,"全站变灰");
		mRem.put(FTP_PASSWORD," FTP密码");
		mRem.put(SITE_CLOSERULE,"站点关闭规则");
		mRem.put(SITE_KEYS,"站点关键字");
		mRem.put(STATIC_CONTENTPAGE,"内容页静态化");
		mRem.put(NO_ADDWATEMARKIMG,"不加水印图片的大小");
		mRem.put(FTP_PORT," FTP端口号");
		mRem.put(DBUSER_NAME,"数据库用户名");
		mRem.put(WEBROOT_PATH," FTP网站根目录");
		mRem.put(TIMING_BAKDB,"定期自动备份网站数据库信息");
		mRem.put(SITE_CLOSETIME,"定时关闭日期(每天：X时X分；每周：1234567；每月：1-31；每年：X月X日）");
		mRem.put(TIMING_BAKDBTIME,"定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）");
		mRem.put(SITE_STATUS,"站点状态");
		mRem.put(IS_ENABLESTATISTICS,"是否开启站点访问量统计");

		for(String s:mKey.keySet()){
			ConfigEntity cf=new ConfigEntity();
			cf.setConfKey(s);
			cf.setConfName(mKey.get(s));
			cf.setConfValue(mVal.get(s));
			cf.setRemark(mRem.get(s));
			cf.setOrder(0);
			cf.setGroup(getGroup(s));
			this.saveEntity(cf);
			
		}

	}
	private String getGroup(String key){
		//siteName siteKeys siteIntro siteStatus pageTemplateId siteGray
		if(key.contains("siteName") || key.contains("siteKeys") || key.contains("siteIntro") || key.contains("siteStatus") || key.contains("pageTemplateId") || key.contains("siteGray")){
			return "网站基础信息";
		}else if(key.contains("siteOpenRule")||key.contains("isEnableStatistics") || key.contains("siteOpenType") || key.contains("siteOpenTime") || key.contains("siteCloseType") || key.contains("pageTemplateId") || key.contains("siteCloseTime")){
			return "网站高级设置";
		}else if(key.contains("makeStaticPage") || key.contains("staticRootPage") || key.contains("staticNodePage") || key.contains("staticContentPage") || key.contains("staticFormPage") || key.contains("staticOtherPage")){
			return "站点静态设置";
		}else if(key.contains("addIimageWatemark") || key.contains("watemarkImage") || key.contains("watemarkOpacity") || key.contains("noAddWatemarkImg") || key.contains("watemarkPosition")){
			return "图片水印设置";
		}else if(key.contains("imageAutoCompress") || key.contains("imageAutoConvert") || key.contains("videoAutoConvToFlv") ){
			return "图片转换设置";
		}else if(key.contains("timingBakSite") || key.contains("timingBakSiteType") || key.contains("timingBakSiteTime") ){
			return "站点备份设置";
		}else if(key.contains("dbType") || key.contains("dbUrl") || key.contains("dbName") || key.contains("dbUserName") || key.contains("dbPassword")
				 || key.contains("timingBakDb") || key.contains("timingBakDbType") || key.contains("timingBakDbTime") || key.contains("timingClearDb")
		|| key.contains("timingClearDbType")|| key.contains("timingClearDbTime")		 
		){
			return "数据库连接和备份";
		}else if(key.contains("autoPublishFtpServer") || key.contains("ftpAddress") || key.contains("ftpPort") || key.contains("ftpUserName")
				 || key.contains("ftpPassword") || key.contains("webRootPath")){
			return "图片转换设置";
		}else{
			return "其他";
		}
	
	}
}
