package com.ibpd.henuocms.timmer;

import java.io.UnsupportedEncodingException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.JobExecutionException;
/**
 * 初始化数据库部分
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:40:32
 */
public class InitDBListener implements ServletContextListener {
	private DBHelper job=new DBHelper();
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		job.stopServer();
	}

	public void contextInitialized(ServletContextEvent arg0) {
		try { 
			job.execute();
		} catch (JobExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
