package com.ibpd.henuocms.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.henuocms.entity.UserEntity;
import com.ibpd.henuocms.entity.UserRoleEntity;
import com.ibpd.shopping.assist.InterfaceUtil;
/**
 * 用户角色的扩展类
 * @author mg by qq:349070443
 *
 */
public class ExtUserRoleEntity extends UserRoleEntity {
	private Boolean checked=false;
	private String userName;
	public ExtUserRoleEntity(UserEntity u){
		this.setUserId(u.getId());
		this.setUserName(u.getUserName());
	}
	public ExtUserRoleEntity(UserRoleEntity ur){
		try {
			InterfaceUtil.swap(ur, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserName() {
		return userName;
	}
	public void setChecked(Boolean ck) {
		this.checked = ck;
	}
	public Boolean getChecked() {
		return checked;
	}

}
