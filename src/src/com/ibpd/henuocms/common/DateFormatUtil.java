package com.ibpd.henuocms.common;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

/**
* 格式化时间类
* @author mg
*/
public class DateFormatUtil{
	/**
	 * 输出格式: 2015-4-16
	 * @param d
	 * @return
	 */
    public static String formatToYYYYMMDD(Date d){
    	return DateFormat.getDateInstance(DateFormat.DEFAULT).format(d);
    }
    /**
     * 2015年4月16日 星期六
     * @param d
     * @return
     */
    public static String formatToYYYYMMDDWEEK(Date d){
    	return DateFormat.getDateInstance(DateFormat.FULL).format(d);
    }
    /**
     * 返回12小时制的带时间的函数 yyyy-MM-dd hh:mm:ss
     * @param d
     * @return
     */
    public static String formatToYYYYMMDDHHMMSSBy12Hours(Date d){
    	 java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
         String s = format1.format(d);
         return s;
    }
    /**
     * 返回24小时制的带时间的函数 yyyy-MM-dd hh:mm:ss
     * @param d
     * @return
     */
    public static String formatToYYYYMMDDHHMMSSBy24Hours(Date d){
    	return (new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(d);
    }
    /**
     * 返回当前日期的字符串：yyyyMMddhhmmss
     * @param d
     * @return
     */
    public static String formatToFullString(Date d){
    	 java.text.DateFormat format2 = new java.text.SimpleDateFormat("yyyyMMddhhmmss");
         return format2.format(d);
    }
    public static void main(String []args){
        Date d = new Date();
        String s;
          
        /** Date类的格式: Sat Apr 16 13:17:29 CST 2006 */
        System.out.println(d);
          
        System.out.println("******************************************");   
        
        /** getDateInstance() */ 
        /** 输出格式: 2006-4-16 */
        s = DateFormat.getDateInstance().format(d);
        System.out.println(s);
        
        /** 输出格式: 2006-4-16 */
        s = DateFormat.getDateInstance(DateFormat.DEFAULT).format(d);
        System.out.println(s);
        
        /** 输出格式: 2006年4月16日 星期六 */
        s = DateFormat.getDateInstance(DateFormat.FULL).format(d);
        System.out.println(s);
        
        /** 输出格式: 2006-4-16 */
        s = DateFormat.getDateInstance(DateFormat.MEDIUM).format(d);
        System.out.println(s);
        
        /** 输出格式: 06-4-16 */
        s = DateFormat.getDateInstance(DateFormat.SHORT).format(d);
        System.out.println(s);
        
        /** 输出格式: 2006-01-01 00:00:00 */
        java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        s = format1.format(new Date());
        System.out.println(s);
        
        /** 输出格式: 2006-01-01 01:00:00 */
        System.out.println((new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(new Date()));
        
        /** 输出格式: 2006-01-01 13:00:00 */
        System.out.println((new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()));

        /** 输出格式: 20060101000000***/
        java.text.DateFormat format2 = new java.text.SimpleDateFormat("yyyyMMddhhmmss");
        s = format2.format(new Date());
        System.out.println(s); 
        Date t=DateFormatUtil.getdate(2);
        System.out.println(DateFormatUtil.formatToYYYYMMDD(t));
    } 
    /**
     * 获取当前日期的前几天或后几天的方法
     * @param i
     * @return
     */
    public static Date getdate(int i)
    {
        Date dat = null;
        Calendar cd = Calendar.getInstance();
        cd.add(Calendar.DATE, i);
        dat = cd.getTime();
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp date = Timestamp.valueOf(dformat.format(dat));
        return date;
    }
}    