package com.ibpd.shopping.assist;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;

import com.ibpd.shopping.entity.CartEntity;


/**
 * 购物车对象，独立出此对象是为了以后的方便操作，当业务进行扩展的时候不会导致系统混乱。
 * 
 * @author huangf
 * 
 */
public class ExtCartInfo implements Serializable {
	static final java.text.DecimalFormat df =new   java.text.DecimalFormat("#.00");
	
	private List<CartEntity> productList;// 购物车中商品列表
//	private String productTotal;//商品总金额
	private String amount;// 合计总金额，也就是用户最终需要支付的金额
	private int totalExchangeScore;//总计所需积分
	
	//private List<AddressEntity> addressList;//用户配送地址信息
	@Deprecated
	//private AddressEntity address;//客户配送信息
	//private String defaultAddessID;//用户的默认地址ID

	public List<CartEntity> getProductList() {
		if(productList==null){
			productList = new LinkedList<CartEntity>();
		}
		return productList;
	}

	public void setProductList(List<CartEntity> productList) {
		this.productList = productList;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
//	public AddressEntity getAddress() {
//		if(address==null){
//			address = new AddressEntity();
//		}
//		return address;
//	}
//
//	public void setAddress(AddressEntity address) {
//		this.address = address;
//	}

	/**
	 * 购物车汇总计算总金额
	 * @return
	 */
	public void totalCacl(){
		double _amount = 0;
		int _totalExchangeScore = 0;
		for(int i=0;i<getProductList().size();i++){
			CartEntity p = getProductList().get(i);
			
//			//积分商城的商品不参与金额计算
//			if(p.getExchangeScore() > 0){
				_totalExchangeScore += p.getScore();// * p.getBuyCount();
//				continue;
//			}
//			_productTotal += Double.valueOf(p.getNowPrice()) * p.getBuyCount();
			_amount += Double.valueOf(p.getSpecPrice()) * p.getBuyCount();
		}
		
		this.totalExchangeScore = _totalExchangeScore;
//		this.productTotal = df.format(_productTotal);
		if(_amount!=0){
			this.amount = df.format(_amount);
		}else{
			this.amount = "0.00";
		}
	}



//	public String getDefaultAddessID() {
//		return defaultAddessID;
//	}
//
//	public void setDefaultAddessID(String defaultAddessID) {
//		this.defaultAddessID = defaultAddessID;
//	}
//
//	public List<AddressEntity> getAddressList() {
//		return addressList;
//	}
//
//	public void setAddressList(List<AddressEntity> addressList) {
//		this.addressList = addressList;
//	}

	public int getTotalExchangeScore() {
		return totalExchangeScore;
	}

	public void setTotalExchangeScore(int totalExchangeScore) {
		this.totalExchangeScore = totalExchangeScore;
	}

	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString();
	}
	public void clear(){
		try {
			InterfaceUtil.swap(new ExtCartInfo(), this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
