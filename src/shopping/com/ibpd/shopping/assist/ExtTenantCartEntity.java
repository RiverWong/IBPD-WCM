package com.ibpd.shopping.assist;

import java.util.ArrayList;
import java.util.List;

import com.ibpd.shopping.entity.CartEntity;

public class ExtTenantCartEntity {

	private Long tenantId;
	private String tenantName;
	private Float amount;
	private List<CartEntity> productList=new ArrayList<CartEntity>();
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public List<CartEntity> getProductList() {
		return productList;
	}
	public void setProductList(List<CartEntity> productList) {
		this.productList = productList;
	}
}
