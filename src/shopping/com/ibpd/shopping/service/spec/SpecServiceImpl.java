package com.ibpd.shopping.service.spec;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.SpecEntity;
@Service("specService")
public class SpecServiceImpl extends BaseServiceImpl<SpecEntity> implements ISpecService {
	public SpecServiceImpl(){
		super();
		this.tableName="SpecEntity";
		this.currentClass=SpecEntity.class;
		this.initOK();
	}

	public List<SpecEntity> getProductSpecList(Long productId) {
		String hql="from "+getTableName()+" where productId="+productId;
		return getList(hql,null);
	}
}
