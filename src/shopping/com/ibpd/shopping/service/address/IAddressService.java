package com.ibpd.shopping.service.address;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.AddressEntity;

public interface IAddressService extends IBaseService<AddressEntity> {
	List<AddressEntity> getAccountAddressList(String account);
	void initDemoData();
}
