package com.ibpd.shopping.service.orderShip;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.OrderShipEntity;

public interface IOrderShipService extends IBaseService<OrderShipEntity> {
	OrderShipEntity getOrderShipByOrderId(Long orderId);
	void deleteByOrderId(Long orderId);
}
